package es.thalesalv.runescape.game.player.controlers;

import es.thalesalv.runescape.game.RegionBuilder;
import es.thalesalv.runescape.game.WorldObject;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.player.content.construction.House;
import es.thalesalv.runescape.game.player.content.construction.House.Room;
import es.thalesalv.runescape.game.player.content.construction.House.RoomReference;

public class HouseControler extends Controler {
	
	private House house;
	private int[] boundChuncks;
	
	@Override
	public void start() {
		house = new House();
		boundChuncks = RegionBuilder.findEmptyChunkBound(8, 8); 
		house.constructHouse(boundChuncks, false);
		player.setNextWorldTile(new WorldTile(boundChuncks[0]*8 + 35, boundChuncks[1]*8 + 35,0));
	//	player.setNextWorldTile(new WorldTile(3564, 3288, 0));
	}//3564,3288
	
	boolean remove = true;
	/**
	 * return process normaly
	 */
	@Override
	public boolean processObjectClick5(WorldObject object) {
		house.previewRoom(player, boundChuncks, new RoomReference(Room.PARLOUR, 4, 5, 0, 0), remove = !remove);
		return true;
	}

}
