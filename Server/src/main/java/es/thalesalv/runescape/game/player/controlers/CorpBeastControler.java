package es.thalesalv.runescape.game.player.controlers;

import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.ForceTalk;
import es.thalesalv.runescape.game.Graphics;
import es.thalesalv.runescape.game.WorldObject;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.tasks.WorldTask;
import es.thalesalv.runescape.game.tasks.WorldTasksManager;
import es.thalesalv.runescape.Settings;

public class CorpBeastControler extends Controler {

	@Override
	public void start() {

	}

	@Override
	public boolean processObjectClick1(WorldObject object) {
		if (object.getId() == 37929 || object.getId() == 38811) {
			removeControler();
			player.stopAll();
			player.setNextWorldTile(new WorldTile(2970, 4384, player.getPlane()));
			return false;
		}
		return true;
	}

	@Override
	public void magicTeleported(int type) {
		removeControler();
	}

	@Override
	public boolean sendDeath() {
		WorldTasksManager.schedule(new WorldTask() {
			int loop;

			@Override
			public void run() {
				int Phylactery = 29003;
				if (player.getInventory().containsItem(Phylactery, 1)) {
					//getInventory().deleteItem(29003, 1);
					//heal(990);
					//setNextAnimation(new Animation(-1));
					//stop();
				if (loop == 0) {
					player.setNextAnimation(new Animation(836));
					player.setNextGraphics(new Graphics(3264));
				} else if (loop == 1) {
					player.getPackets().sendGameMessage("Oh dear, you have died.");
					player.setNextGraphics(new Graphics(3264));
				} else if (loop == 3) {
					player.heal(990);
					player.setNextGraphics(new Graphics(3264));
				} else if (loop == 4) {
					player.setNextAnimation(new Animation(-1));
					player.setNextGraphics(new Graphics(3264));
					//player.setNextForceTalk(new ForceTalk("Dying is for Pussy's becouse i have Soul Phylactery!"));
					player.getInventory().deleteItem(29003, 1);
					stop();
				}
				} else {
				if (loop == 0) {
					player.setNextAnimation(new Animation(836));
				} else if (loop == 1) {
					player.getPackets().sendGameMessage("Oh dear, you have died.");
				} else if (loop == 3) {
					Player killer = player.getMostDamageReceivedSourcePlayer();
					if (killer != null) {
						killer.removeDamage(player);
						killer.increaseKillCount(player);
					}
					//player.sendItemsOnDeath(player);
					player.getEquipment().init();
					player.getInventory().init();
					player.reset();
					player.setNextWorldTile(new WorldTile(Settings.RESPAWN_PLAYER_LOCATION));
					player.setNextAnimation(new Animation(-1));
				} else if (loop == 4) {
					removeControler();
					player.getPackets().sendMusic(90);
					stop();
				}
				}
				loop++;
			}
		}, 0, 1);
		return false;
	}

	@Override
	public boolean login() {
		return false; // so doesnt remove script
	}

	@Override
	public boolean logout() {
		return false; // so doesnt remove script
	}

}
