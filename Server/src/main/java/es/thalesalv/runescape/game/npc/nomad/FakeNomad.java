package es.thalesalv.runescape.game.npc.nomad;

import es.thalesalv.runescape.game.Hit;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.npc.NPC;

@SuppressWarnings("serial")
public class FakeNomad extends NPC {
	
	private Nomad nomad;
	
	public FakeNomad(WorldTile tile, Nomad nomad) {
		super(8529, tile, -1, true, true);
		this.nomad = nomad;
		setAtMultiArea(true);
	}
	
	@Override
	public void handleIngoingHit(Hit hit) {
		nomad.destroyCopy(this);
	}
	
}
