package es.thalesalv.runescape.game.player.cutscenes.actions;

import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.player.Player;

public class PlayerAnimationAction extends CutsceneAction {

	private Animation anim;

	public PlayerAnimationAction(Animation anim, int actionDelay) {
		super(-1, actionDelay);
		this.anim = anim;
	}

	@Override
	public void process(Player player, Object[] cache) {
		player.setNextAnimation(anim);
	}

}
