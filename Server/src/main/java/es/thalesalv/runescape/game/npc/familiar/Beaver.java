package es.thalesalv.runescape.game.npc.familiar;

import es.thalesalv.runescape.game.WorldObject;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.actions.Summoning.Pouches;
//import es.thalesalv.runescape.game.player.actions.Woodcutting;
//import es.thalesalv.runescape.game.player.actions.Woodcutting.TreeDefinitions;
import es.thalesalv.runescape.game.player.skills.Woodcutting;
import es.thalesalv.runescape.game.player.skills.Woodcutting.TreeDefinitions;

public class Beaver extends Familiar {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9181393770444014076L;

	public Beaver(Player owner, Pouches pouch, WorldTile tile,
			int mapAreaNameHash, boolean canBeAttackFromOutOfArea) {
		super(owner, pouch, tile, mapAreaNameHash, canBeAttackFromOutOfArea);
	}

	@Override
	public String getSpecialName() {
		return "Multichop";
	}

	@Override
	public String getSpecialDescription() {
		return "Chops a tree, giving the owner its logs. There is also a chance that random logs may be produced.";
	}

	@Override
	public int getBOBSize() {
		return 0;
	}

	@Override
	public int getSpecialAmount() {
		return 3;
	}

	@Override
	public SpecialAttack getSpecialAttack() {
		return SpecialAttack.OBJECT;
	}

	@Override
	public boolean submitSpecial(Object context) {
		WorldObject object = (WorldObject) context;
		//getOwner().getActionManager().setAction(
			//	new Woodcutting(object, TreeDefinitions.Normal_Tree));
		return true;
	}
}
