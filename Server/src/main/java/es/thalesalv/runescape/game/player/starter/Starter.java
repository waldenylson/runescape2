package es.thalesalv.runescape.game.player.starter;

import es.thalesalv.runescape.game.player.Player;

/**
 * This class handles the giving of a starter kit.
 * 
 * @author Emperial
 * 
 */
public class Starter {

	public static final int MAX_STARTER_COUNT = 3;

	public static void appendStarter(Player player) {
		String ip = player.getSession().getIP();
		int count = StarterMap.getSingleton().getCount(ip);
		player.starter = 1;
		if (count >= MAX_STARTER_COUNT) {
			return;
		}
		player.getInterfaceManager().sendInterface(771);
		//player.getCutscenesManager().play(5);
		player.getInventory().addItem(1856, 1); // book
		player.getInventory().addItem(995, 20000); // cash
		player.getInventory().addItem(330, 100); // cash
	//	player.softreset = true; 

		player.getHintIconsManager().removeUnsavedHintIcon();
		player.getMusicsManager().reset();
		player.getCombatDefinitions().setAutoRelatie(true);
		player.getCombatDefinitions().refreshAutoRelatie();
		player.getCutscenesManager().play(5);
		StarterMap.getSingleton().addIP(ip);
	}

}