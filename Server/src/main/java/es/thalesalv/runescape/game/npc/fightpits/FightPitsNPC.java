package es.thalesalv.runescape.game.npc.fightpits;

import java.util.ArrayList;

import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.Graphics;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.minigames.FightPits;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.player.Player;

@SuppressWarnings("serial")
public class FightPitsNPC extends NPC {


	public FightPitsNPC(int id, WorldTile tile) {
		super(id, tile, -1, true, true);
		setForceMultiArea(true);
		setNoDistanceCheck(true);
	}
	
	@Override
	public void sendDeath(Entity source) {
		setNextGraphics(new Graphics(2924 + getSize()));
		super.sendDeath(source);
	}

	@Override
	public ArrayList<Entity> getPossibleTargets() {
		ArrayList<Entity> possibleTarget = new ArrayList<Entity>();
		for(Player player : FightPits.arena)
			possibleTarget.add(player);
		return possibleTarget;
	}

}
