package es.thalesalv.runescape.game.player.content;

import java.util.TimerTask;

import es.thalesalv.runescape.cores.CoresManager;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.tasks.WorldTask;
import es.thalesalv.runescape.game.tasks.WorldTasksManager;
import es.thalesalv.runescape.utils.Logger;
import es.thalesalv.runescape.utils.Utils;

public final class FadingScreen {

	private FadingScreen() {
		
	}
	
	public static void fade(final Player player, final Runnable event) {
		unfade(player, fade(player), event);
	}
	
	public static void unfade(final Player player, long startTime, final Runnable event) {
		long leftTime =  2500 - (Utils.currentTimeMillis() - startTime);
		if(leftTime > 0) {
			CoresManager.fastExecutor1.schedule(new TimerTask() {
				@Override
				public void run() {
					try {
						unfade(player, event);
					} catch (Throwable e) {
						Logger.handle(e);
					}
				}
				
			}, leftTime);
		}else
			unfade(player, event);
	}
	
	
	public static void unfade(final Player player, Runnable event) {
		event.run();
		WorldTasksManager.schedule(new WorldTask() {

			@Override
			public void run() {
				player.getInterfaceManager().sendFadingInterface(170);
				CoresManager.fastExecutor1.schedule(new TimerTask() {
					@Override
					public void run() {
						try {
							player.getInterfaceManager().closeFadingInterface();
						} catch (Throwable e) {
							Logger.handle(e);
						}
					}
				}, 2000);
			}
			
		});
	}
	
	public static long fade(Player player) {
		player.getInterfaceManager().sendFadingInterface(115);
		return Utils.currentTimeMillis();
	}
	
}
