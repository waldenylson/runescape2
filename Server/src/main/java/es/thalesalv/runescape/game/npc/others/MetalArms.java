package es.thalesalv.runescape.game.npc.others;

import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.game.player.controlers.Metals;
import es.thalesalv.runescape.utils.Utils;

@SuppressWarnings("serial")
public class MetalArms extends NPC {

	private Metals metals;

	public MetalArms(int id, WorldTile tile, Metals metals) {
		super(id, tile, -1, true, true);
		this.metals = metals;
	}

	@Override
	public void sendDeath(Entity source) {
		if(metals != null) {
			metals.targetDied();
			metals = null;
		}
		super.sendDeath(source);
	}
	
	@Override
	public double getMeleePrayerMultiplier() {
		return getId() != 2030 ? 0 : Utils.random(3) == 0 ? 1 : 0;
	}
	
	
	public void disapear() {
		metals = null;
		finish();
	}
	@Override
	public void finish() {
		if(hasFinished())
			return;
		if(metals != null) {
			metals.targetFinishedWithoutDie();
			metals = null;
		}
		super.finish();
	}

}