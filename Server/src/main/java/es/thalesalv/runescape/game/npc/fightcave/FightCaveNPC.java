package es.thalesalv.runescape.game.npc.fightcave;

import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.npc.NPC;
import es.thalesalv.runescape.utils.Misc;

public class FightCaveNPC extends NPC
{

    public FightCaveNPC(int id, WorldTile tile)
    {
        super(id, tile, Misc.getNameHash("FightCaves"), false, true);
    }
}