package es.thalesalv.runescape.game.player;

import es.thalesalv.runescape.game.player.dialogues.Dialogue;
import es.thalesalv.runescape.game.player.dialogues.DialogueHandler;

public class DialogueManager {

	private Player player;
	Dialogue lastDialogue;
	
	public DialogueManager(Player player) {
		this.player = player;
	}
	
	public void startDialogue(Object key, Object... parameters) {
		if(!player.getControlerManager().useDialogueScript(key))
			return;
		if(lastDialogue != null)
			lastDialogue.finish();
		lastDialogue = DialogueHandler.getDialogue(key);
		if(lastDialogue == null)
			return;
		lastDialogue.parameters = parameters;
		lastDialogue.setPlayer(player);
		lastDialogue.start();
	}

	public void continueDialogue(int interfaceId, int componentId) {
		if(lastDialogue == null)
			return;
		lastDialogue.run(interfaceId, componentId);
	}
	
	public void finishDialogue() {
		if(lastDialogue == null)
			return;
		lastDialogue.finish();
		lastDialogue = null;
		if(player.getInterfaceManager().containsChatBoxInter())
			player.getInterfaceManager().closeChatBoxInterface();
	}

}
