package es.thalesalv.runescape.game.npc;

import es.thalesalv.runescape.game.Entity;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.player.skills.Dungeon;

@SuppressWarnings("serial")
public class DungeonBoss extends NPC {

	private Dungeon dungeon;
	
	public DungeonBoss(int id, WorldTile tile, Dungeon dungeon) {
		super(id, tile, 0, true, true);
		this.dungeon = dungeon;
	}
	
	@Override
	public void sendDeath(Entity source) {
		super.sendDeath(source);
		dungeon.openStairs();
	}
}
