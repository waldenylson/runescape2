package es.thalesalv.runescape.game.player.dialogues;

import java.util.HashMap;

public final class DialogueHandler {

	private static final HashMap<Object, Class<Dialogue>> handledDialogues = new HashMap<Object, Class<Dialogue>>();
	
	@SuppressWarnings("unchecked")
	public static final void init() {
		try {
			Class<Dialogue> value1 = (Class<Dialogue>) Class.forName(LevelUp.class.getCanonicalName());
			handledDialogues.put("LevelUp", value1);
			Class<Dialogue> value2 = (Class<Dialogue>) Class.forName(ZarosAltar.class.getCanonicalName());
			handledDialogues.put("ZarosAltar", value2);
			Class<Dialogue> value3 = (Class<Dialogue>) Class.forName(ClimbNoEmoteStairs.class.getCanonicalName());
			handledDialogues.put("ClimbNoEmoteStairs", value3);
			Class<Dialogue> value4 = (Class<Dialogue>) Class.forName(Banker.class.getCanonicalName());
			handledDialogues.put("Banker", value4);
			Class<Dialogue> value5 = (Class<Dialogue>) Class.forName(DestroyItemOption.class.getCanonicalName());
			handledDialogues.put("DestroyItemOption", value5);
			Class<Dialogue> value6 = (Class<Dialogue>) Class.forName(FremennikShipmaster.class.getCanonicalName());
			handledDialogues.put("FremennikShipmaster", value6);
			Class<Dialogue> value7 = (Class<Dialogue>) Class.forName(DungeonExit.class.getCanonicalName());
			handledDialogues.put("DungeonExit", value7);
			Class<Dialogue> value8 = (Class<Dialogue>) Class.forName(test.class.getCanonicalName());
			handledDialogues.put("test", value8);
			Class<Dialogue> value9 = (Class<Dialogue>) Class
					.forName(BarrowsD.class.getCanonicalName());
			handledDialogues.put("BarrowsD", value9);
			Class<Dialogue> value10 = (Class<Dialogue>) Class
					.forName(CookingD.class.getCanonicalName());
			handledDialogues.put("CookingD", value10);
			Class<Dialogue> value11 = (Class<Dialogue>) Class
					.forName(SmeltingD.class.getCanonicalName());
			handledDialogues.put("SmeltingD", value11);
			Class<Dialogue> value12 = (Class<Dialogue>) Class
					.forName(Transportation.class.getCanonicalName());
			handledDialogues.put("Transportation", value12);
			Class<Dialogue> value13 = (Class<Dialogue>) Class
					.forName(WildernessDitch.class.getCanonicalName());
			handledDialogues.put("WildernessDitch", value13);
			Class<Dialogue> value14 = (Class<Dialogue>) Class
					.forName(LunarAltar.class.getCanonicalName());
			handledDialogues.put("LunarAltar", value14);
			Class<Dialogue> value15 = (Class<Dialogue>) Class
					.forName(AncientAltar.class.getCanonicalName());
			handledDialogues.put("AncientAltar", value15);
			handledDialogues.put("LanderD", (Class<Dialogue>) Class.forName(LanderDialouge.class.getCanonicalName()));
			//handledDialogues.put("MiningGuildDwarf", (Class<Dialogue>) Class.forName(MiningGuildDwarf.class.getCanonicalName()));
			Class<Dialogue> value16 = (Class<Dialogue>) Class
					.forName(TeleportMinigame.class.getCanonicalName());
			handledDialogues.put("TeleportMinigame", value16);
			Class<Dialogue> value17 = (Class<Dialogue>) Class
					.forName(TeleportBosses.class.getCanonicalName());
			handledDialogues.put("TeleportBosses", value17);
			Class<Dialogue> value18 = (Class<Dialogue>) Class
					.forName(TeleportTraining.class.getCanonicalName());
			handledDialogues.put("TeleportTraining", value18);
			Class<Dialogue> value19 = (Class<Dialogue>) Class
					.forName(Ozan.class.getCanonicalName());
			handledDialogues.put("Ozan", value19);
			Class<Dialogue> value20 = (Class<Dialogue>) Class
					.forName(BorkEnter.class.getCanonicalName());
			handledDialogues.put("BorkEnter", value20);
            Class<Dialogue> value22 = (Class<Dialogue>) Class
					.forName(Varnis.class.getCanonicalName());
			handledDialogues.put("Varnis", value22);
			Class<Dialogue> value23 = (Class<Dialogue>) Class
					.forName(Veliaf.class.getCanonicalName());
			handledDialogues.put("Veliaf", value23);
			Class<Dialogue> value24 = (Class<Dialogue>) Class
					.forName(SetSkills.class.getCanonicalName());
			handledDialogues.put("SetSkills", value24);
			Class<Dialogue> value25 = (Class<Dialogue>) Class
					.forName(Pool.class.getCanonicalName());
			handledDialogues.put("Pool", value25);
			Class<Dialogue> value26 = (Class<Dialogue>) Class
					.forName(DismissD.class.getCanonicalName());
			handledDialogues.put("DismissD", value26);
			Class<Dialogue> value27 = (Class<Dialogue>) Class
					.forName(MrEx.class.getCanonicalName());
			handledDialogues.put("MrEx", value27);
			Class<Dialogue> value28 = (Class<Dialogue>) Class
					.forName(NewHomeGuide.class.getCanonicalName());
			handledDialogues.put("NewHomeGuide", value28);
			Class<Dialogue> value29 = (Class<Dialogue>) Class
					.forName(Talk.class.getCanonicalName());
			handledDialogues.put("Talk", value29);
//slayer
			Class<Dialogue> value34 = (Class<Dialogue>) Class
					.forName(ChristmasCrackerD.class.getCanonicalName());
			handledDialogues.put("ChristmasCrackerD", value34);
			Class<Dialogue> value36 = (Class<Dialogue>) Class
					.forName(CrashedStar.class.getCanonicalName());
			handledDialogues.put("CrashedStar", value36);
			Class<Dialogue> value37 = (Class<Dialogue>) Class
					.forName(WoodcuttingMaster.class.getCanonicalName());
			handledDialogues.put("WoodcuttingMaster", value37);
			Class<Dialogue> value38 = (Class<Dialogue>) Class
					.forName(LumbyGaurds.class.getCanonicalName());
			handledDialogues.put("LumbyGaurds", value38);
			Class<Dialogue> value39 = (Class<Dialogue>) Class
					.forName(MeleeTutor.class.getCanonicalName());
			handledDialogues.put("MeleeTutor", value39);
			Class<Dialogue> value40 = (Class<Dialogue>) Class
					.forName(Buy.class.getCanonicalName());
			handledDialogues.put("Buy", value40);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static final void reload() {
		handledDialogues.clear();
		init();
	}
	
	public static final Dialogue getDialogue(Object key) {
		Class<Dialogue> classD = handledDialogues.get(key);
		if(classD == null)
			return null;
		try {
			return classD.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private DialogueHandler() {
		
	}
}
