package es.thalesalv.runescape;

import java.util.concurrent.TimeUnit;

import org.jboss.netty.channel.ChannelException;

import es.thalesalv.runescape.cache.Cache;
import es.thalesalv.runescape.cache.loaders.ItemDefinitions;
import es.thalesalv.runescape.cache.loaders.ItemEquipIds;
import es.thalesalv.runescape.cache.loaders.NPCDefinitions;
import es.thalesalv.runescape.cache.loaders.ObjectDefinitions;
import es.thalesalv.runescape.cores.CoresManager;
import es.thalesalv.runescape.game.Region;
import es.thalesalv.runescape.game.RegionBuilder;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.area.AreaManager;
import es.thalesalv.runescape.game.minigames.soulwars.SoulWars;
import es.thalesalv.runescape.game.npc.combat.CombatScriptsHandler;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.content.FishingSpotsHandler;
import es.thalesalv.runescape.game.player.content.grandexchange.GrandExchange;
import es.thalesalv.runescape.game.player.controlers.ControlerHandler;
import es.thalesalv.runescape.game.player.cutscenes.CutscenesHandler;
import es.thalesalv.runescape.game.player.dialogues.DialogueHandler;
import es.thalesalv.runescape.net.ServerChannelHandler;
import es.thalesalv.runescape.utils.IPBanL;
import es.thalesalv.runescape.utils.ItemBonuses;
import es.thalesalv.runescape.utils.ItemExamines;
import es.thalesalv.runescape.utils.Logger;
import es.thalesalv.runescape.utils.MapAreas;
import es.thalesalv.runescape.utils.MapContainersXteas;
import es.thalesalv.runescape.utils.MusicHints;
import es.thalesalv.runescape.utils.NPCBonuses;
import es.thalesalv.runescape.utils.NPCCombatDefinitionsL;
import es.thalesalv.runescape.utils.NPCDrops;
import es.thalesalv.runescape.utils.NPCSpawning;
import es.thalesalv.runescape.utils.NPCSpawns;
import es.thalesalv.runescape.utils.ObjectSpawns;
import es.thalesalv.runescape.utils.PkRank;
import es.thalesalv.runescape.utils.SerializableFilesManager;
import es.thalesalv.runescape.utils.ShopsHandler;
import es.thalesalv.runescape.utils.WorldList;
import es.thalesalv.runescape.utils.huffman.Huffman;

public final class Launcher {

	public static final void main(String[] args) throws Exception {
		long currentTime = System.currentTimeMillis();
		Logger.log("Launcher", "Iniciando cache...");
		Cache.init();
		ItemEquipIds.init();
		Huffman.init();
		Logger.log("Launcher", "Iniciando arquivos de dados...");
		MapContainersXteas.init();
		MapAreas.init();
		ObjectSpawns.init();
		IPBanL.init();
		PkRank.init();
		ItemExamines.init();
		AreaManager.init();
		NPCSpawns.init();
		SoulWars.init();
		WorldList.getWorldList();
		NPCBonuses.init();
		NPCDrops.init();
		GrandExchange.init();
		MusicHints.init();
		ShopsHandler.init();
		NPCCombatDefinitionsL.init();
		ItemBonuses.init();
		Logger.log("Launcher", "Iniciando pontos de pesca...");
		FishingSpotsHandler.init();
		Logger.log("Launcher", "Iniciando diálogos...");
		DialogueHandler.init();
		Logger.log("Launcher", "Iniciando controladoras...");
		ControlerHandler.init();
		Logger.log("Launcher", "Iniciando cenas...");
		CutscenesHandler.init();
		Logger.log("Launcher", "Iniciando scripts de combate de NPCs...");
		CombatScriptsHandler.init();
		Logger.log("Launcher", "Iniciando gerenciados de núcleos...");
		CoresManager.init();
		Logger.log("Launcher", "Iniciando mundo...");
		World.init();
		Logger.log("Launcher", "Iniciando fábrica de regiões...");
		RegionBuilder.init();
		// Logger.log("Launcher", "Iniciando painel de controle...");
		// Panel frame = new Panel();
		// frame.setVisible(true);
		Logger.log("Launcher", "Iniciando controladora de canal de servidor...");
		try {
			ServerChannelHandler.init();
			NPCSpawning.spawnNPCS();
		} catch (ChannelException e) {
			e.printStackTrace();
			Logger.log("Launcher", "Falha ao iniciar controladora de canal de servidor. Cancelando...");
			shutdown();
			return;
		}
		Logger.log("Launcher",
				"Servidor iniciado após " + (System.currentTimeMillis() - currentTime) + " milissegundos.");
		addCleanMemoryTask();
		addAccountsSavingTask();
	}

	private static final void addCleanMemoryTask() {
		CoresManager.slowExecutor.scheduleWithFixedDelay(new Runnable() {
			@Override
			public void run() {
				if (Runtime.getRuntime().freeMemory() < Settings.MIN_FREE_MEM_ALLOWED)
					cleanMemory();
			}
		}, 0, 10, TimeUnit.MINUTES);
	}

	public static void saveFiles() {
		for (Player player : World.getPlayers()) {
			if (player == null || !player.hasStarted() || player.hasFinished())
				continue;
			SerializableFilesManager.savePlayer(player);
		}
		IPBanL.save();
		PkRank.save();
	}

	private static final void addAccountsSavingTask() {
		CoresManager.slowExecutor.scheduleWithFixedDelay(new Runnable() {
			@Override
			public void run() {
				try {
					saveFiles();
				} catch (Throwable e) {
					Logger.handle(e);
				}

			}
		}, 0, 15, TimeUnit.MINUTES);
	}

	/*
	 * public static final void saveAccounts() { for(Player player :
	 * World.getPlayers()) { if(player == null || !player.hasStarted() ||
	 * player.hasFinished()) continue; SerializableFilesManager.savePlayer(player);
	 * } }
	 */

	public static final void cleanMemory() {
		ItemDefinitions.clearItemsDefinitions();
		NPCDefinitions.clearNPCDefinitions();
		ObjectDefinitions.clearObjectDefinitions();
		for (Region region : World.getRegions().values())
			region.removeMapFromMemory();
		System.gc();
	}

	public static final void shutdown() {
		try {
			ServerChannelHandler.shutdown();
			CoresManager.shutdown();
		} finally {
			System.exit(0);
		}
	}

	public static final void restart() {
		// TODO
	}

	private Launcher() {

	}

}
