package es.thalesalv.runescape;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import es.thalesalv.runescape.game.Animation;
import es.thalesalv.runescape.game.ForceTalk;
import es.thalesalv.runescape.game.Hit;
import es.thalesalv.runescape.game.Hit.HitLook;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.content.Magic;
import es.thalesalv.runescape.utils.IPBanL;
import es.thalesalv.runescape.utils.Utils;

public class Panel extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Panel frame = new Panel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Panel() {
		setTitle("RuneScape");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 408, 456);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(87, 25, 194, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblUsername = new JLabel("Usuário");
		lblUsername.setBounds(156, 11, 92, 14);
		contentPane.add(lblUsername);
		
		JLabel lblPunishment = new JLabel("Punição");
		lblPunishment.setBounds(73, 60, 104, 14);
		contentPane.add(lblPunishment);
		
		/*JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(10, 372, 372, 35);
		contentPane.add(progressBar);
		progressBar.setIndeterminate(true);*/
		
		JButton btnIpban = new JButton("IPBan");
		btnIpban.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IPban();
			}
		});
		btnIpban.setBounds(0, 85, 89, 23);
		contentPane.add(btnIpban);
		
		JButton btnMute = new JButton("Mutar");
		btnMute.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mute();
			}
		});
		btnMute.setBounds(99, 85, 89, 23);
		contentPane.add(btnMute);
		
		JButton btnBan = new JButton("Banir");
		btnBan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ban();
			}
		});
		btnBan.setBounds(0, 119, 89, 23);
		contentPane.add(btnBan);
		
		JButton btnJail = new JButton("Prender");
		btnJail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				jail();
			}
		});
		btnJail.setBounds(0, 153, 89, 23);
		contentPane.add(btnJail);
		
		JButton btnKill = new JButton("Matar");
		btnKill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kill();
			}
		});
		btnKill.setBounds(99, 119, 89, 23);
		contentPane.add(btnKill);
		
		JButton btnFreezeunfreeze = new JButton("Congelar");
		btnFreezeunfreeze.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				freeze();
			}
		});
		btnFreezeunfreeze.setBounds(99, 153, 89, 23);
		contentPane.add(btnFreezeunfreeze);
		
		JLabel lblItemManagment = new JLabel("Gerenciamento de item");
		lblItemManagment.setBounds(261, 60, 104, 14);
		contentPane.add(lblItemManagment);
		
		JButton btnGiveItem = new JButton("Dar item");
		btnGiveItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				giveItem();
			}
		});
		btnGiveItem.setBounds(204, 85, 89, 23);
		contentPane.add(btnGiveItem);
		
		JButton btnTakeItem = new JButton("Pegar item");
		btnTakeItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				takeItem();
			}
		});
		btnTakeItem.setBounds(303, 85, 89, 23);
		contentPane.add(btnTakeItem);
		
		JButton btnGiveAll = new JButton("Dar tudo");
		btnGiveAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				giveAll();
			}
		});
		btnGiveAll.setBounds(204, 119, 89, 23);
		contentPane.add(btnGiveAll);
		
		JButton btnTakeAll = new JButton("Pegar tudo");
		btnTakeAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				takeAll();
			}
		});
		btnTakeAll.setBounds(303, 119, 89, 23);
		contentPane.add(btnTakeAll);
		
		JLabel lblTeleportation = new JLabel("Teleporte");
		lblTeleportation.setBounds(73, 206, 104, 14);
		contentPane.add(lblTeleportation);
		
		JButton btnTeleport = new JButton("Teleportar");
		btnTeleport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				teleport();
			}
		});
		btnTeleport.setBounds(57, 231, 89, 23);
		contentPane.add(btnTeleport);
		
		JButton btnSendhome = new JButton("Teleportar tudo");
		btnSendhome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				teleAll();
			}
		});
		btnSendhome.setBounds(57, 265, 89, 23);
		contentPane.add(btnSendhome);
		
		JLabel lblFunPanel = new JLabel("Painel de Diversão");
		lblFunPanel.setBounds(261, 177, 56, 14);
		contentPane.add(lblFunPanel);
		
		JButton btnMakeDance = new JButton("Fazer dançar");
		btnMakeDance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				makeDance();
			}
		});
		btnMakeDance.setBounds(231, 202, 114, 23);
		contentPane.add(btnMakeDance);
		
		JButton btnDanceAll = new JButton("Dançar todos");
		btnDanceAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				danceAll();
			}
		});
		btnDanceAll.setBounds(231, 236, 114, 23);
		contentPane.add(btnDanceAll);
		
		JButton btnForceChat = new JButton("Forçar chat");
		btnForceChat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				forceChat();
			}
		});
		btnForceChat.setBounds(231, 270, 114, 23);
		contentPane.add(btnForceChat);
		
		JButton btnSmite = new JButton("Obliterar");
		btnSmite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				smite();
			}
		});
		btnSmite.setBounds(231, 304, 114, 23);
		contentPane.add(btnSmite);
		
		JButton btnFuckUp = new JButton("Foder");
		btnFuckUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fuckUp();
			}
		});
		btnFuckUp.setBounds(231, 338, 114, 23);
		contentPane.add(btnFuckUp);
		
		JButton btnNewButton = new JButton("Desligar servidor");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				shutdown();
			}
		});
		btnNewButton.setBounds(10, 299, 194, 62);
		contentPane.add(btnNewButton);
		
	}
	
	public String getUsernameInput() {
		return textField.getText();
	}
	
	public void ban() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
	if (target != null) {
		target.getSession().getChannel().close();
		World.removePlayer(target);
		System.out.println("Console: Banido com sucesso "
				+ name + ".");
		JOptionPane.showMessageDialog(null, name + " banido com sucesso", "Console", JOptionPane.PLAIN_MESSAGE);
	} else {
		JOptionPane.showMessageDialog(null, name +" não existe", "Console", JOptionPane.ERROR_MESSAGE);
		System.out.println("Console: "
			+ Utils.formatPlayerNameForDisplay(name) + " não existe");
		}
	}

	public void forceChat() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
	if (target != null) {
		String chat = JOptionPane.showInputDialog("O que quer que ele diga?");
		target.setNextForceTalk(new ForceTalk(chat));
		System.out.println("Console: Forçando "+name+" a dizer "+chat+"!");
		JOptionPane.showMessageDialog(null, "Forçando "+name+" a dizer "+chat+"!", "Console", JOptionPane.PLAIN_MESSAGE);
	} else {
		JOptionPane.showMessageDialog(null, name+" não existe", "Console", JOptionPane.ERROR_MESSAGE);
		System.out.println("Console: "
			+ Utils.formatPlayerNameForDisplay(name) + " não existe");
		}
	}
	
	public void shutdown() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
		String sht = JOptionPane.showInputDialog("Desligar delay?");
		int delay = Integer.parseInt(sht);
		World.safeShutdown(false, delay);
		System.out.println("Console: Desligando servidor!");
		JOptionPane.showMessageDialog(null, "Desligando servidor!", "Console", JOptionPane.PLAIN_MESSAGE);
	}
	
	public void makeDance() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
	if (target != null) {
		String id = JOptionPane.showInputDialog("ID da dança");
		//target.setNextAnimation(new Animation(7071));
		target.setNextAnimation(new Animation(Integer.parseInt(id)));
		System.out.println("Console: VOcê fez "+name+" dançar!");
		JOptionPane.showMessageDialog(null, "Você fez "+name+" dançar!", "Console", JOptionPane.PLAIN_MESSAGE);
	} else {
		JOptionPane.showMessageDialog(null, name+" não existe", "Console", JOptionPane.ERROR_MESSAGE);
		System.out.println("Console: "
			+ Utils.formatPlayerNameForDisplay(name) + " não existe");
		}
	}
	
	public void smite() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
	if (target != null) {
		target.setPrayerDelay(999999999);
		System.out.println("Console: Você obliterou "+name+"!");
		JOptionPane.showMessageDialog(null, "VOcê obliterou "+name+"!", "Console", JOptionPane.PLAIN_MESSAGE);
	} else {
		JOptionPane.showMessageDialog(null, name+" não existe", "Console", JOptionPane.ERROR_MESSAGE);
		System.out.println("Console: "
			+ Utils.formatPlayerNameForDisplay(name) + " não existe");
		}
	}
	
	public void fuckUp() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
	if (target != null) {
		target.setPrayerDelay(999999999);
		target.addFreezeDelay(999999999);
		for (int i = 0; i < 10; i++)
			target.getCombatDefinitions().getBonuses()[i] = 0;
		for (int i = 14; i < target.getCombatDefinitions().getBonuses().length; i++)
			target.getCombatDefinitions().getBonuses()[i] = 0;
		System.out.println("Console: Você fodeu com "+name+"!");
		JOptionPane.showMessageDialog(null, "Você fodeu com "+name+"!", "Console", JOptionPane.PLAIN_MESSAGE);
	} else {
		JOptionPane.showMessageDialog(null, name+" não existe", "Console", JOptionPane.ERROR_MESSAGE);
		System.out.println("Console: "
			+ Utils.formatPlayerNameForDisplay(name) + " não existe");
		}
	}
	
	public void IPban() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
	if (target != null) {
		boolean loggedIn = true;
		IPBanL.ban(target, loggedIn);
		System.out.println("Console: IP banido com sucesso "
				+ name + ".");
		JOptionPane.showMessageDialog(null, "IP de "+name + "banido com sucesso", "Console", JOptionPane.PLAIN_MESSAGE);
	} else {
		JOptionPane.showMessageDialog(null, name+" não existe!", "Console", JOptionPane.ERROR_MESSAGE);
		System.out.println("Console: "
			+ Utils.formatPlayerNameForDisplay(name) + " não existe!");
		}
	}
	
	public void mute() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
	if (target != null) {
		target.setMuted(Utils.currentTimeMillis()
				+ (48 * 60 * 60 * 1000));
		System.out.println("Console: Mutado "
				+ name + ".");
		JOptionPane.showMessageDialog(null, name + " mutado com sucesso", "Console", JOptionPane.PLAIN_MESSAGE);
	} else {
		JOptionPane.showMessageDialog(null, name+" não existe!", "Console", JOptionPane.ERROR_MESSAGE);
		System.out.println("Console: "
			+ Utils.formatPlayerNameForDisplay(name) + " não existe!");
		}
	}
	
	public void kill() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
	if (target != null) {
		target.applyHit(new Hit(target, target.getHitpoints(),
				HitLook.REGULAR_DAMAGE));
		target.stopAll();
		System.out.println("Console: "
				+ name + " foi morto.");
		JOptionPane.showMessageDialog(null, name + " foi morto com sucesso", "Console", JOptionPane.PLAIN_MESSAGE);
	} else {
		JOptionPane.showMessageDialog(null, name+" não existe!", "Console", JOptionPane.ERROR_MESSAGE);
		System.out.println("Console: "
			+ Utils.formatPlayerNameForDisplay(name) + " não existe!");
		}
	}
	public void jail() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
	if (target != null) {
		target.setJailed(Utils.currentTimeMillis()
				+ (24 * 60 * 60 * 1000));
		target.getControlerManager().startControler("JailControler");
		System.out.println("Console: "
				+ name + " preso.");
		JOptionPane.showMessageDialog(null, name + " preso com sucesso", "Console", JOptionPane.PLAIN_MESSAGE);
	} else {
		JOptionPane.showMessageDialog(null, name+" não existe!", "Console", JOptionPane.ERROR_MESSAGE);
		System.out.println("Console: "
			+ Utils.formatPlayerNameForDisplay(name) + " não existe!");
		}
	}
	
	public void freeze() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
	if (target != null) {
		target.addFreezeDelay(999999999);
		System.out.println("Console: "
				+ name + " congelado.");
		JOptionPane.showMessageDialog(null, name + " congelado com sucesso", "Console", JOptionPane.PLAIN_MESSAGE);
	} else {
		JOptionPane.showMessageDialog(null, name+" não existe!", "Console", JOptionPane.ERROR_MESSAGE);
		System.out.println("Console: "
			+ Utils.formatPlayerNameForDisplay(name) + " não existe!");
		}
	}
	
	public void giveItem() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
	if (target != null) {
		String id = JOptionPane.showInputDialog("ID do item");
		String quantity = JOptionPane.showInputDialog("Quantidade");
		int item = Integer.parseInt(id);
		int amount = Integer.parseInt(quantity);
		target.getInventory().addItem(item, amount);
		System.out.println("Console: Item dado a "
				+ name + ".");
		JOptionPane.showMessageDialog(null, "Item dado a "+name, "Console", JOptionPane.PLAIN_MESSAGE);
	} else {
		JOptionPane.showMessageDialog(null, name+" não existe!", "Console", JOptionPane.ERROR_MESSAGE);
		System.out.println("Console: "
			+ Utils.formatPlayerNameForDisplay(name) + " não existe!");
		}
	}
	
	public void teleport() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
	if (target != null) {
		String x = JOptionPane.showInputDialog("Coordenada X");
		String y = JOptionPane.showInputDialog("Coordenada Y");
		String h = JOptionPane.showInputDialog("Nível de altura");
		int coordx = Integer.parseInt(x);
		int coordy = Integer.parseInt(y);
		int height = Integer.parseInt(h);
		Magic.sendNormalTeleportSpell(target, 0, 0, new WorldTile(coordx, coordy, height));
		System.out.println("Console: "+name+" teleportado para "+coordx+", "+coordy+", "+height);
		JOptionPane.showMessageDialog(null, "Console: "+name+" teleportado para "+coordx+", "+coordy+", "+height, "Console", JOptionPane.PLAIN_MESSAGE);
	} else {
		JOptionPane.showMessageDialog(null, name+" não existe!", "Console", JOptionPane.ERROR_MESSAGE);
		System.out.println("Console: "
			+ Utils.formatPlayerNameForDisplay(name) + " não existe!");
		}
	}
	
	public void teleAll() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
		String x = JOptionPane.showInputDialog("Coordenada X");
		String y = JOptionPane.showInputDialog("Coordenada Y");
		String h = JOptionPane.showInputDialog("Nível de altura");
		int coordx = Integer.parseInt(x);
		int coordy = Integer.parseInt(y);
		int height = Integer.parseInt(h);
		for (Player teleall : World.getPlayers()) {
		Magic.sendNormalTeleportSpell(teleall, 0, 0, new WorldTile(coordx, coordy, height));
		}
		System.out.println("Console: Todos teleportados para "+coordx+", "+coordy+", "+height);
		JOptionPane.showMessageDialog(null, "Console: Todos teleportados para "+coordx+", "+coordy+", "+height, "Console", JOptionPane.PLAIN_MESSAGE);
}
	
	public void danceAll() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
		for (Player danceAll : World.getPlayers()) {
			danceAll.setNextAnimation(new Animation(7071));
		}
		System.out.println("Console: Fazendo todos dançarem!");
		JOptionPane.showMessageDialog(null, "Console: Fazendo todos dançarem!", "Console", JOptionPane.PLAIN_MESSAGE);
}
	
	public void takeItem() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
	if (target != null) {
		String id = JOptionPane.showInputDialog("ID do item");
		String quantity = JOptionPane.showInputDialog("Quantidade");
		int item = Integer.parseInt(id);
		int amount = Integer.parseInt(quantity);
		target.getInventory().deleteItem(item, amount);
		System.out.println("Console: Item "+item+" pego de "
				+ name + ".");
		JOptionPane.showMessageDialog(null, "Item "+item+" pego de "+name, "Console", JOptionPane.PLAIN_MESSAGE);
	} else {
		JOptionPane.showMessageDialog(null, name+" não existe!", "Console", JOptionPane.ERROR_MESSAGE);
		System.out.println("Console: "
			+ Utils.formatPlayerNameForDisplay(name) + " não existe!");
		}
	}
	
	public void giveAll() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
		String id = JOptionPane.showInputDialog("ID do item");
		String quantity = JOptionPane.showInputDialog("QUantidade");
		int item = Integer.parseInt(id);
		int amount = Integer.parseInt(quantity);
		for (Player giveall : World.getPlayers()) {
		giveall.getInventory().addItem(item, amount);
		}
		System.out.println("Console: Item "+item+" dado a todos os jogadores");
		JOptionPane.showMessageDialog(null, "Item "+item+" dado a todos os jogadores", "Console", JOptionPane.PLAIN_MESSAGE);
	}
	
	public void takeAll() {
		String name = getUsernameInput();
	Player target = World.getPlayerByDisplayName(name);
		String id = JOptionPane.showInputDialog("ID do item");
		String quantity = JOptionPane.showInputDialog("Quantidade");
		int item = Integer.parseInt(id);
		int amount = Integer.parseInt(quantity);
		for (Player takeall : World.getPlayers()) {
		takeall.getInventory().deleteItem(item, amount);
		}
		System.out.println("Console: Item "+item+" pego de todos os jogadores");
		JOptionPane.showMessageDialog(null, "Item "+item+" pego de todos os jogadores", "Console", JOptionPane.PLAIN_MESSAGE);
	}
}