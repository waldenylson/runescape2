package es.thalesalv.runescape.net.decoders.handlers;

import java.util.ArrayList;
import es.thalesalv.runescape.io.InputStream;
import es.thalesalv.runescape.utils.Utils;
import es.thalesalv.runescape.cache.loaders.ObjectDefinitions;
import es.thalesalv.runescape.game.World;
import es.thalesalv.runescape.game.WorldObject;
import es.thalesalv.runescape.game.WorldTile;
import es.thalesalv.runescape.game.item.Item;
import es.thalesalv.runescape.game.player.CoordsEvent;
import es.thalesalv.runescape.game.player.Inventory;
import es.thalesalv.runescape.game.player.Player;
//import es.thalesalv.runescape.game.player.skills.Mining;

/**
 * Simplifies the event which occurs when a certain object is clicked
 * (OBJECT_ClICK_1 packet) so that the end-user of the source will not need to
 * deal with the hassle of adding ifs and other code in WorldPacketsDecoder. <br />
 * I haven't taken the time to copy all the implemented IDs from
 * WorldPacketsDecoder, however. (Besides Mining)
 * 
 * @author Cyber Sheep
 */

public class ObjectHandler {

	/**
	 * Fix for "Nothing interesting happens" message being sent when an object
	 * is clicked
	 */


	public static void handleItemOnObject(final Player player, final WorldObject object, final int interfaceId, final Item item) {
		final int itemId = item.getId() | 3 << 16;
		final ObjectDefinitions objectDef = object.getDefinitions();
		player.setCoordsEvent(new CoordsEvent(object, new Runnable() {
			@Override
			public void run() {
				/*if (!player.getControlerManager().handleItemOnObject(object, item))
				    return;*/
				if (object.getId() == 48803 && itemId == 954) {
					if (player.isKalphiteLairSetted())
						return;
					player.getInventory().deleteItem(954, 1);
					player.setKalphiteLair();
				} else if (object.getId() == 48802 && itemId == 954) {
					if (player.isKalphiteLairEntranceSetted())
						return;
					player.getInventory().deleteItem(954, 1);
					player.setKalphiteLairEntrance();
				} 
				player.faceObject(object);	
						System.out.println("Item on object: " + item.getId());
				
			}
		}, objectDef.getSizeX(), objectDef.getSizeY(), object.getRotation()));
	}

	public static boolean handleDoor(Player player, WorldObject object,
			long timer) {
		if (World.isSpawnedObject(object))
			return false;
		WorldObject openedDoor = new WorldObject(object.getId(),
				object.getType(), object.getRotation() + 1, object.getX(),
				object.getY(), object.getPlane());
		if (object.getRotation() == 0)
			openedDoor.moveLocation(-1, 0, 0);
		else if (object.getRotation() == 1)
			openedDoor.moveLocation(0, 1, 0);
		else if (object.getRotation() == 2)
			openedDoor.moveLocation(1, 0, 0);
		else if (object.getRotation() == 3)
			openedDoor.moveLocation(0, -1, 0);
		if (World.removeObjectTemporary(object, timer, true)) {
			player.faceObject(openedDoor);
			World.spawnObjectTemporary(openedDoor, timer);
			return true;
		}
		return false;
	}

	public static boolean handleDoor(Player player, WorldObject object) {
		return handleDoor(player, object, 60000);
	}
}