package es.thalesalv.runescape.net.decoders.handlers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import es.thalesalv.runescape.game.item.Item;
import es.thalesalv.runescape.game.minigames.clanwars.FfaZone;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.content.ItemConstants;
import es.thalesalv.runescape.game.player.content.grandexchange.GrandExchange;
import es.thalesalv.runescape.utils.Utils;

public class ButtonHandler {

	public static void sendRemove(Player player, int slotId) {
		if (slotId >= 15)
			return;
		player.stopAll(false, false);
		Item item = player.getEquipment().getItem(slotId);
		if (item == null
				|| !player.getInventory().addItem(item.getId(),
						item.getAmount()))
			return;
		player.getEquipment().getItems().set(slotId, null);
		player.getEquipment().refresh(slotId);
		player.getAppearence().generateAppearenceData();
		/*if (Runecrafting.isTiara(item.getId()))
			player.getPackets().sendConfig(491, 0);*/
		if (slotId == 3)
			player.getCombatDefinitions().desecreaseSpecialAttack(0);
		

}
	
	public static void openItemsKeptOnDeath(Player player) {
		player.getInterfaceManager().sendInterface(102);
		sendItemsKeptOnDeath(player, player.isAtWild() ? true : false);
	}
	
	public static void sendItemsKeptOnDeath(Player player, boolean wilderness) {
		boolean skulled = player.hasSkull();
		boolean inFfa = FfaZone.inArea(player);
		@SuppressWarnings("unused")
		// TODO
		boolean inRiskArea = FfaZone.inRiskArea(player);
		Integer[][] slots = getItemSlotsKeptOnDeath(player, wilderness,
				skulled, player.getPrayer().usingPrayer(0, 10)
						|| player.getPrayer().usingPrayer(1, 0));
		Item[][] items = getItemsKeptOnDeath(player, slots);
		long riskedWealth = 0;
		long carriedWealth = 0;
		for (Item item : items[1]) {
			if (item == null)
				continue;
			carriedWealth = riskedWealth += GrandExchange
					.getPrice(item.getId()) * item.getAmount();
		}
		for (Item item : items[0]) {
			if (item == null)
				continue;
			carriedWealth += GrandExchange.getPrice(item.getId())
					* item.getAmount();
		}
		if (slots[0].length > 0) {
			for (int i = 0; i < slots[0].length; i++)
				player.getPackets().sendConfig(1054 + i, slots[0][i]);
			player.getPackets().sendConfig(1056, slots[0].length);
		} else {
			player.getPackets().sendConfig(1054, -1);
			player.getPackets().sendConfig(1056, 1);
		}
		player.getPackets().sendConfig(1055, (wilderness || inFfa) ? 1 : 0);
		if (!inFfa)
			player.getPackets().sendConfig(2159, player.hasSkull() ? 1 : 0);
		StringBuffer text = new StringBuffer();
		text.append("Items kept on death:").append("<br><br>");
		for (Item item : items[0]) {
			text.append(item.getName()).append("<br>").append("<br>");
		}
		text.append("<br>")
				.append("<br>")
				.append("Carried wealth:")
				.append("<br>")
				.append(carriedWealth > Integer.MAX_VALUE ? "Too high!" : Utils
						.getFormattedNumber((int) carriedWealth, ','))
				.append("<br>")
				.append("<br>")
				.append("Risked wealth:")
				.append("<br>")
				.append(riskedWealth > Integer.MAX_VALUE ? "Too high!" : Utils
						.getFormattedNumber((int) riskedWealth, ','))
				.append("<br>").append("<br>");
		text.append("Respawn point:").append("<br>").append("Edgeville bank.");
		player.getPackets().sendGlobalString(276, text.toString());
	}

	public static Item[][] getItemsKeptOnDeath(Player player, Integer[][] slots) {
		ArrayList<Item> droppedItems = new ArrayList<Item>();
		ArrayList<Item> keptItems = new ArrayList<Item>();
		for (int i : slots[0]) { // items kept on death
			Item item = i >= 16 ? player.getInventory().getItem(i - 16)
					: player.getEquipment().getItem(i - 1);
			if (item == null) // shouldnt
				continue;
			if (item.getAmount() > 1) {
				droppedItems.add(new Item(item.getId(), item.getAmount() - 1));
				item.setAmount(1);
			}
			keptItems.add(item);
		}
		for (int i : slots[1]) { // items droped on death
			Item item = i >= 16 ? player.getInventory().getItem(i - 16)
					: player.getEquipment().getItem(i - 1);
			if (item == null) // shouldnt
				continue;
			droppedItems.add(item);
		}
		for (int i : slots[2]) { // items protected by default
			Item item = i >= 16 ? player.getInventory().getItem(i - 16)
					: player.getEquipment().getItem(i - 1);
			if (item == null) // shouldnt
				continue;
			keptItems.add(item);
		}
		return new Item[][] { keptItems.toArray(new Item[keptItems.size()]),
				droppedItems.toArray(new Item[droppedItems.size()]) };

	}

	public static Integer[][] getItemSlotsKeptOnDeath(final Player player,
			boolean atWilderness, boolean skulled, boolean protectPrayer) {
		ArrayList<Integer> droppedItems = new ArrayList<Integer>();
		ArrayList<Integer> protectedItems = new ArrayList<Integer>();
		ArrayList<Integer> lostItems = new ArrayList<Integer>();
		boolean inRiskArea = FfaZone.inRiskArea(player);
		for (int i = 1; i < 44; i++) {
			Item item = i >= 16 ? player.getInventory().getItem(i - 16)
					: player.getEquipment().getItem(i - 1);
			if (item == null)
				continue;
			int stageOnDeath = item.getDefinitions().getStageOnDeath();
			/*if (ItemConstants.keptOnDeath(item) && atWilderness)
				protectedItems.add(i);
			else */if (!atWilderness && stageOnDeath == 1)
				protectedItems.add(i);
			else if (stageOnDeath == -1)
				lostItems.add(i);
			else
				droppedItems.add(i);
		}
		int keptAmount = (player.hasSkull() || inRiskArea) ? 0 : 3;
		if (protectPrayer)
			keptAmount++;
		if (droppedItems.size() < keptAmount)
			keptAmount = droppedItems.size();
		Collections.sort(droppedItems, new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				Item i1 = o1 >= 16 ? player.getInventory().getItem(o1 - 16)
						: player.getEquipment().getItem(o1 - 1);
				Item i2 = o2 >= 16 ? player.getInventory().getItem(o2 - 16)
						: player.getEquipment().getItem(o2 - 1);
				int price1 = i1 == null ? 0
						: GrandExchange.getPrice(i1.getId());
				int price2 = i2 == null ? 0
						: GrandExchange.getPrice(i2.getId());
				if (price1 > price2)
					return -1;
				if (price1 < price2)
					return 1;
				return 0;
			}

		});
		Integer[] keptItems = new Integer[keptAmount];
		for (int i = 0; i < keptAmount; i++) {
			keptItems[i] = droppedItems.remove(0);
		}
		return new Integer[][] {
				keptItems,
				droppedItems.toArray(new Integer[droppedItems.size()]),
				protectedItems.toArray(new Integer[protectedItems.size()]),
				atWilderness ? new Integer[0] : lostItems
						.toArray(new Integer[lostItems.size()]) };

	}
}
