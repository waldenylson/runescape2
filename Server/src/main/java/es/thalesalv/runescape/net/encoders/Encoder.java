package es.thalesalv.runescape.net.encoders;

import es.thalesalv.runescape.net.Session;

public abstract class Encoder {

	public Session session;

	public Encoder(Session session) {
		this.session = session;
	}

}
