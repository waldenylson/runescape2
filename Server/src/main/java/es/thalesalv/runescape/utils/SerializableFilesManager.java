package es.thalesalv.runescape.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


import es.thalesalv.runescape.utils.Logger;
import es.thalesalv.runescape.utils.SerializableFilesManager;
import es.thalesalv.runescape.game.player.Player;
import es.thalesalv.runescape.game.player.content.grandexchange.Offer;
import es.thalesalv.runescape.game.player.content.grandexchange.OfferHistory;

public class SerializableFilesManager {
	
	public synchronized static final boolean containsPlayer(String username) {
		return new File("data/characters/"+username+".p").exists();
	}
	private static final String GE_OFFERS = "grandExchangeOffers.ser";
    private static final String GE_OFFERS_HISTORY = "grandExchangeOffersTrack.ser";
    private static final String GE_PRICES = "grandExchangePrices.ser";
    
	public synchronized static Player loadPlayer(String username) {
		try {
			return (Player) loadSerializedFile(new File("data/characters/"+username+".p"));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public synchronized static void savePlayer(Player player) {
		try {
			storeSerializableClass(player, new File("data/characters/"+player.getUsername()+".p"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
    public static final Object loadSerializedFile(File f) throws IOException, ClassNotFoundException {
    	if(!f.exists())
    		return null;
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(f));
        Object object = in.readObject();
        in.close();
        return object;
    }
    
    public static final void storeSerializableClass(Serializable o, File f) throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(f));
        out.writeObject(o);
        out.close();
    }
	
	private SerializableFilesManager() {
		
	}

    @SuppressWarnings("unchecked")
	public static synchronized HashMap<Long, Offer> loadGEOffers() {
    	if (new File(GE_OFFERS).exists()) {
    	    try {
    	    	return (HashMap<Long, Offer>)loadSerializedFile(new File(GE_OFFERS));
    	    }
    	    catch (Throwable t) {
    	    	Logger.handle(t);
    		return null;
    	    }
    	}
    	else {
    	    return new HashMap<Long, Offer>();
    	}
    }
    
    public static synchronized ArrayList<OfferHistory> loadGEHistory() {
    	if (new File(GE_OFFERS_HISTORY).exists()) {
    	    try {
    		return (ArrayList<OfferHistory>)loadSerializedFile(new File(GE_OFFERS_HISTORY));
    	    }
    	    catch (Throwable t) {
    		Logger.handle(t);
    		return null;
    	    }
    	}
    	else {
    	    return new ArrayList<OfferHistory>();
    	}
    }
    
    @SuppressWarnings("unchecked")
	public static synchronized HashMap<Integer, Integer> loadGEPrices() {
    	if (new File(GE_PRICES).exists()) {
    	    try {
    		return (HashMap<Integer, Integer>)loadSerializedFile(new File(GE_PRICES));
    	    }
    	    catch (Throwable t) {
    		Logger.handle(t);
    		return null;
    	    }
    	}
    	else {
    	    return new HashMap<Integer, Integer>();
    	}
    }
    
    public static synchronized void saveGEOffers(HashMap<Long, Offer> offers) {
    	try {
    	    SerializableFilesManager.storeSerializableClass(offers, new File(GE_OFFERS));
    	}
    	catch (Throwable t) {
    	    Logger.handle(t);
    	}
    }
    
    public static synchronized void saveGEHistory(ArrayList<OfferHistory> history) {
		try {
		    SerializableFilesManager.storeSerializableClass(history, new File(GE_OFFERS_HISTORY));
		}
		catch (Throwable t) {
		    Logger.handle(t);
		}
    }
    
    public static synchronized void saveGEPrices(HashMap<Integer, Integer> prices) {
		try {
		    SerializableFilesManager.storeSerializableClass(prices, new File(GE_PRICES));
		}
		catch (Throwable t) {
		    Logger.handle(t);
		}
    }
    
	
}
