
import java.awt.Color;

public class ClientSettings {

	public static String getCacheDir() {
		return System.getProperty("user.home") + "/.ostavaCache";
	}
//https://www.dropbox.com/s/mhm2keicggnsmo5/ostavacache.zip
	public static final String cacheLink = "http://dl.dropbox.com/s/mhm2keicggnsmo5/ostavacache.zip";
	public static int cacheRevision = 1;
	public static final String ip = "ostava.no-ip.biz";
	public static final int port = 43594;
	public static final String background_music_name = "Pest Control";
	public static final String serverName = "Ostava";
	public static final String websiteLink = "";
	public static final Color startLoadClientBackgroundColor = Color.black;
	public static final boolean disable_server_list = true;
	public static final boolean disable_ukeys_check = true;
	public static final boolean disable_keys_request = true;

	/*
	 * WEBCLIENT
	 */
	//for webclient all you have to do is jar and sign
	//I RECOMMENT YOU TO USE JARMAKER APP FOR THAT http://download.cnet.com/JAR-Maker/3000-2213_4-10588876.html
	//main-class: client
	//applet-class:client too
	
	/*
	 * dont worry even trying hacking, this user is limited :)
	 */
	public static final String server_list_database = "127.0.0.1/servers"; //ip, database
	public static final String server_list_user = "ostava"; //user
	public static final String server_list_pass = "ostava"; //pass
}
