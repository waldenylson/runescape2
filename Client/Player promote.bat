@echo off
"%JAVA_HOME%\bin\java.exe" -server -Xms512m -XX:NewSize=32m -XX:MaxPermSize=128m -XX:+UseConcMarkSweepGC -XX:+ExplicitGCInvokesConcurrent -XX:+AggressiveOpts -cp bin;data/libs/*;data/libs/slf4j/*; bin/RSAKeyGen
pause